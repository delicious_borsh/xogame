package xoGame;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.JCSystem;
import javacard.framework.Util;
import javacard.security.RandomData;

public class xoGame extends Applet {

    // valid CLA value
    final static byte VALID_CLA = (byte)       0xB0;
    
    // valid INS values
    final static byte NEW_GAME = (byte)        0x10;
    final static byte NEXT_MOVE = (byte)       0x20;
    final static byte GET_RANDOM = (byte)      0x30;

    // values
    final static byte X_SYMBOL = (byte)        0x58;
    final static byte O_SYMBOL = (byte)        0x4F;
    final static byte EMPTY_SYMBOL = (byte)    0x00;
    final static byte BORDER_SYMBOL = (byte)   0x23;
    
    final static byte X_IS_CHOOSEN = (byte)    0x00;
    
    byte messageToShow = (byte) 0x00; // if not 0x00, bytes from 0x01 to 0x04 represent the following messages:

    final static byte X_MOVE_MESSAGE = (byte) 0x01;
    final static byte O_MOVE_MESSAGE = (byte) 0x02;
    final static byte X_WON_MESSAGE = (byte) 0x03;
    final static byte O_WON_MESSAGE = (byte) 0x04;
    
    final static byte [] xMoveMessage = {(byte) 0x58, (byte) 0x5F, (byte) 0x4D, (byte) 0x4F, (byte) 0x56, (byte) 0x45};
    final static byte [] oMoveMessage = {(byte) 0x4F, (byte) 0x5F, (byte) 0x4D, (byte) 0x4F, (byte) 0x56, (byte) 0x45};
    final static byte [] xWonMessage =  {(byte) 0x58, (byte) 0x5F, (byte) 0x57, (byte) 0x4F, (byte) 0x4E, (byte) 0x00};
    final static byte [] oWonMessage =  {(byte) 0x4F, (byte) 0x5F, (byte) 0x57, (byte) 0x4F, (byte) 0x4E, (byte) 0x00};
    
    byte [] field = {EMPTY_SYMBOL, EMPTY_SYMBOL, EMPTY_SYMBOL,
                     EMPTY_SYMBOL, EMPTY_SYMBOL, EMPTY_SYMBOL,
                     EMPTY_SYMBOL, EMPTY_SYMBOL, EMPTY_SYMBOL,};

    // state vars
    boolean gameHasBegun = false;
    boolean gameIsWon = false;
    boolean playersMove = false; // TRUE for player, FALSE for card
    boolean playerBegins = false; // If player choose the X's then they can make the first move  
    
    RandomData random = RandomData.getInstance(RandomData.ALG_SECURE_RANDOM);
    byte[] rndBuffer = JCSystem.makeTransientByteArray(JCSystem.CLEAR_ON_DESELECT, (byte) 1);
    
    public static void install(byte[] bArray, short bOffset, byte bLength) {
        new xoGame().register(bArray, (short) (bOffset + 1), bArray[bOffset]);
    }

    public void process(APDU apdu) {
        if (selectingApplet()) {
            return;
        }

        byte[] buf = apdu.getBuffer();
        
        if (buf[ISO7816.OFFSET_CLA] != VALID_CLA)
            ISOException.throwIt(ISO7816.SW_CLA_NOT_SUPPORTED);
        
        switch (buf[ISO7816.OFFSET_INS]) {
            case (byte) NEW_GAME:
                new_game(apdu);
                if(!playerBegins) {
                    non_player_move();
                }
                printField(apdu);
                break;
            case (byte) NEXT_MOVE:
                if (gameHasBegun){
                    player_move(apdu);
                    non_player_move();
                }
                printField(apdu);
                break;    
            default:
                ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
        }
    }
    
    public void new_game(APDU apdu) {
        byte[] buf = apdu.getBuffer(); 
        clearTheField();
        gameHasBegun = true;
        if (buf[ISO7816.OFFSET_P2] == X_IS_CHOOSEN) { // 00 for X, anything else for O
            playerBegins = true;
            messageToShow = X_MOVE_MESSAGE;
        }
        else { 
            playerBegins = false;
        }
    }
    
    public void player_move(APDU apdu) {
        byte[] buf = apdu.getBuffer();
        if (!gameHasBegun || gameIsWon) {
            return;
        }
        if (buf[ISO7816.OFFSET_P1] > (byte) 3 || buf[ISO7816.OFFSET_P2] > (byte) 3) {
            return;
        }       
        
        // calculating 1-dimensional coordinates from 2-dimensional ones
        byte i = (byte) (buf[ISO7816.OFFSET_P1] + (buf[ISO7816.OFFSET_P2] - 1) * 3 - 1);
        
        if (field [i] == EMPTY_SYMBOL) {
            if (playerBegins) {
                field [i] = X_SYMBOL;
                messageToShow = X_MOVE_MESSAGE;
            } else {
                field [i] = O_SYMBOL;
                messageToShow = O_MOVE_MESSAGE;  
            }
        }
        else {
            return;
        }      
    }
    
    public void non_player_move() {
        if (playerBegins) {
            field [optimalMove(O_SYMBOL)] = O_SYMBOL;
        } else {
            field[optimalMove(O_SYMBOL)] = X_SYMBOL;
        }  
    }
    
    public void printField(APDU apdu){
        byte[] buf = apdu.getBuffer();

        for (short i = 0; i < 79; i++) {
            if(i < 5 || (i > 64 && i < 68) || i % 16 == 0 || i % 16 == 4)
                buf[i] = BORDER_SYMBOL;
            else
            buf[i] = (byte) EMPTY_SYMBOL;
        }
        
        buf[17] = field[0];
        buf[18] = field[1];
        buf[19] = field[2];
        buf[33] = field[3];
        buf[34] = field[4];
        buf[35] = field[5];
        buf[49] = field[6];
        buf[50] = field[7];
        buf[51] = field[8];
        
        byte index = 0;
        switch (messageToShow){
            case X_MOVE_MESSAGE:
                for (byte i = 22; i < 28; i++){
                    buf[i] = xMoveMessage[index];
                    index++;
                }
                break;
            case O_MOVE_MESSAGE:
                for (byte i = 22; i < 28; i++){
                    buf[i] = oMoveMessage[index];
                    index++;
                }
                break;
            case X_WON_MESSAGE:
                for (byte i = 22; i < 28; i++){
                    buf[i] = xWonMessage[index];
                    index++;
                }
                break;
            case O_WON_MESSAGE:
                for (byte i = 22; i < 28; i++){
                    buf[i] = oWonMessage[index];
                    index++;
                }
                break;
            case (byte) 0x00:
                break;
        }     
        apdu.setOutgoingAndSend((short)0, (short)80);
    }
    
    public void clearTheField(){
        for (byte i = 0; i < field.length; i++) {
            field[i] = EMPTY_SYMBOL;
        }
    }
    
    public byte optimalMove(byte symbol) {
        for (byte i = 0; i <= field.length; i++) {
            switch (i) {
                case 0: {
                    if ((field[1] == symbol && field[2] == symbol) ||
                            (field[4] == symbol && field[8] == symbol) ||
                            (field[3] == symbol && field[6] == symbol)) {
                        return (byte) 1;
                    }
                }
                case 1: {
                    if ((field[0] == symbol && field[2] == symbol) ||
                            (field[4] == symbol && field[7] == symbol)) {
                        return (byte) 2;
                    }
                }
                case 2: {
                    if ((field[0] == symbol && field[1] == symbol) ||
                            (field[5] == symbol && field[8] == symbol) ||
                            (field[4] == symbol && field[6] == symbol)) {
                        return (byte) 3;
                    }
                }
                case 3: {
                    if ((field[0] == symbol && field[6] == symbol) ||
                            (field[4] == symbol && field[5] == symbol)) {
                        return (byte) 4;
                    }
                }
                case 4: {
                    if ((field[1] == symbol && field[7] == symbol) ||
                            (field[3] == symbol && field[5] == symbol) ||
                            (field[0] == symbol && field[8] == symbol) ||
                            (field[6] == symbol && field[2] == symbol)) {
                        return (byte) 5;
                    }
                }
                case 5: {
                    if ((field[2] == symbol && field[8] == symbol) ||
                            (field[3] == symbol && field[4] == symbol)) {
                        return (byte) 6;
                    }
                }
                case 6: {
                    if ((field[0] == symbol && field[3] == symbol) ||
                            (field[7] == symbol && field[8] == symbol) ||
                            (field[4] == symbol && field[2] == symbol)) {
                        return (byte) 7;
                    }
                }
                case 7: {
                    if ((field[1] == symbol && field[4] == symbol) ||
                            (field[6] == symbol && field[8] == symbol)) {
                        return (byte) 8;
                    }
                }
                case 8: {
                    if ((field[2] == symbol && field[5] == symbol) ||
                            (field[6] == symbol && field[7] == symbol) ||
                            (field[0] == symbol && field[4] == symbol)) {
                        return (byte) 9;
                    }
                }
            }
        }
        return randomMove();
    }
    
    byte randomMove() {
        byte result;
        do {
            result = getRandomByte();
        }
        while(field[result] != EMPTY_SYMBOL);
        return result;
    }
    
    byte getRandomByte() {
        do {
          random.generateData(rndBuffer, (byte) 0, (byte) 1);
        } while ((rndBuffer[0]<0) || (rndBuffer[0]>9));
        return rndBuffer[0];
    }
}

